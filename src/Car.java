public class Car extends Vehicle
{
    @Override
    public void vehicleMethod()
    {
        //super.vehicleMethod();
        System.out.println("I m overridinig Vehicle method - I m CAR");
    }

    public void carMethod()
    {
        System.out.println("I m Car method");
    }
}
