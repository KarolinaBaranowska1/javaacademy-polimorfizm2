import java.util.Date;

public class Main
{

    public static void main(String[] args)
    {

        Vehicle vehicle = new Vehicle();
        Plane plane = new Plane();
        Car car = new Car();
        Ship ship = new Ship();
        Truck truck = new Truck();
        RaceCar raceCar = new RaceCar();

        Vehicle[] vehicles = {vehicle, plane, car, truck, raceCar, ship};

        for(Vehicle v : vehicles)
        {
            if(v instanceof RaceCar)
            {
                ((RaceCar) v).raceCarMethod();
                ((RaceCar) v).carMethod();
                ((RaceCar) v).vehicleMethod();
            } else if(v instanceof Truck)
            {
                ((Truck) v).truckMethod();
                ((Truck) v).carMethod();
                ((Truck) v).vehicleMethod();
            }else if(v instanceof Car)
            {
                ((Car) v).carMethod();
                ((Car) v).vehicleMethod();
            }else if(v instanceof Ship)
            {
                ((Ship) v).shipMethod();
                ((Ship) v).vehicleMethod();
            }else if(v instanceof Plane)
            {
                ((Plane) v).planeMethod();
                ((Plane) v).vehicleMethod();
            }
            else v.vehicleMethod();
        }

    }
}

