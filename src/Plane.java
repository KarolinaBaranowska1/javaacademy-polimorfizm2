public class Plane extends Vehicle
{
    @Override
    public void vehicleMethod()
    {
        //super.vehicleMethod();
        System.out.println("I m overridinig Vehicle method - I m PLANE");
    }

    public void planeMethod()
    {
        System.out.println("I m Plane method");
    }
}
