public class RaceCar extends Car
{
    @Override
    public void carMethod()
    {
        //super.carMethod();
        System.out.println("Hi, Im overriding carMethod from Car class which overrides it from vehicle- I AM RACECAR");
    }

    @Override
    public void vehicleMethod()
    {
        //super.vehicleMethod();
        System.out.println("Hi, Im overriding vehicleMethod from Vehicle class - I AM RACECAR ");
    }

    public void raceCarMethod()
    {
        System.out.println("Hi, I m raceCar method");
    }
}
