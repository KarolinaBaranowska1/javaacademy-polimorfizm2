public class Truck extends Car
{
    @Override
    public void carMethod()
    {
        //super.carMethod();
        System.out.println("Hi, Im overriding carMethod from Car class which overrides it from vehicle- I AM TRUCK");
    }

    @Override
    public void vehicleMethod()
    {
        //super.vehicleMethod();
        System.out.println("Hi, Im overriding vehicleMethod from Vehicle class - I AM TRUCK");
    }

    public void truckMethod()
    {
        System.out.println("Hi, I m truck method");
    }
}
